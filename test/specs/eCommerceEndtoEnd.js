const loginPage = require("../pageObjects/loginPage");

const expectchai = require("chai").expect

describe('Ecommerce End to End Project', async () => {

    it("End to end scenarios", async () => {

        await browser.url("https://rahulshettyacademy.com/loginpagePractise/");
        // await $("#username").setValue("rahulshettyacademy");
        //await $("#password").setValue("learning");
        //await $("#signInBtn").click();

        //Login using Page Objects
        loginPage.login("rahulshettyacademy", "learning");

        const link = await $("*=Checkout");
        await link.waitForExist();

        const product = ['iphone X', 'Blackberry'];
        const cards = await $$("div[class='card h-100']");
        for (let i = 0; i < await cards.length; i++) {
            const card = await cards[i].$("div h4 a");
            if (product.includes(await card.getText())) {
                await cards[i].$(".card-footer button").click();
            }
        }
        await link.click();
        // await browser.pause(9000);

        const productPrices = await $$("//tr/td[4]/strong");
        const sumOfProduct = (await Promise.all(await productPrices.map(async (productPrice) => parseInt((await productPrice.getText()).split(".")[1].trim()))))
            .reduce((acc, price) => acc + price, 0);
        console.log("sum is : " + sumOfProduct);
        const TotalValue = await $("h3 strong").getText();
        const TotalIntValue = parseInt(TotalValue.split(".")[1].trim());
        console.log("Total Value: " + TotalIntValue);
        await expectchai(sumOfProduct).to.equal(TotalIntValue);

        await $(".btn-success").click();
        await $("#country").setValue("ind");
        await $(".lds-ellipsis").waitForExist({ reverse: true });
        await $("=India").click();
        await $("input[type='submit']").click();
        await expect($('.alert-success')).toHaveTextContaining("Success");
    })
});