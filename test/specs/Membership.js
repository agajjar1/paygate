const membershipPage = require("../pageObjects/membershipPage");
const fs = require('fs');
let membershipTestData = JSON.parse(fs.readFileSync('test/testData/membershipForm.json'))

// const expectchai = require("chai").expect

describe('Ecommerce Automation Project', async () => {


    it('Create Membership Request', async () => {
        await browser.url("https://paygate.appsqa.egovja.com/");

        let formData = await membershipPage.getMembershipFormAttributes();

        await membershipPage.applyMembershipbtn.click();
        await browser.maximizeWindow();

        await membershipPage.verifyTabExist.waitForExist();
        const tabsLocators = await membershipPage.tabLocators;

        // Insert values for Company Details

        await $(formData['company_name']).setValue(membershipTestData.company_name);
        await $(formData['company_address']).setValue(membershipTestData.company_address);
        await $(formData['primary_phone']).setValue(membershipTestData.primary_phone);
        await $(formData['secondary_phone']).setValue(membershipTestData.secondary_phone);
        await $(tabsLocators[1]).click();

        // //Insert values for Contact 1 information
        await $(formData['contact1_firstname']).setValue(membershipTestData.contact1_firstname);
        await $(formData['contact1_lastname']).setValue(membershipTestData.contact1_lastname);
        await $(formData['contact1_title']).setValue(membershipTestData.contact1_title);
        await $(formData['contact1_email']).setValue(membershipTestData.contact1_email);
        await $(formData['contact1_phone']).setValue(membershipTestData.contact1_phone);
        await $(tabsLocators[2]).click();

        //Insert value for Contatct 2 Information
        await $(formData['contact2_firstname']).setValue(membershipTestData.contact2_firstname);
        await $(formData['contact2_lastname']).setValue(membershipTestData.contact2_lastname);
        await $(formData['contact2_title']).setValue(membershipTestData.contact2_title);
        await $(formData['contact2_email']).setValue(membershipTestData.contact2_email);
        await $(formData['contact2_phone']).setValue(membershipTestData.contact2_phone);
        await $(tabsLocators[3]).click();

        await membershipPage.submitBtn.scrollIntoView();
        await membershipPage.submitBtn.click();

        await expect(membershipPage.toaster).toHaveTextContaining("Membership Request Created Successfully")

    }),
        xit('Edit Membership Request', async () => {
            await browser.url("https://paygate.nginxdev.egovja.com/membership-onboarding-listing");
            const table_data = await $$('tbody[role="rowgroup"] tr');
            await $(table_data[0]).click();


            // it("search table testsuite", async () => {
            //     await browser.url("https://www.rahulshettyacademy.com/seleniumPractise/#/offers");
            //     await $("#search-field").setValue("tomato")
            //     const veggieLocators = await $$("tr td:nth-child(1)");
            //     await expect(veggieLocators).toBeElementsArrayOfSize({ eq: 1 });
            //     console.log(await veggieLocators[0].getText())
            //     await expect(await veggieLocators[0]).toHaveTextContaining("Tomaato")
            // })

            // it("handle multiple tabs", async () => {
            //     await browser.url("https://www.rahulshettyacademy.com/loginpagePractise/");
            //     await $(".blinkingText").click();
            //     const handle = await browser.getWindowHandles();
            //     await browser.switchToWindow(handle[1]);
            //     console.log(await $("h1").getText());
            //     await browser.closeWindow()
            //     await browser.switchToWindow(handle[0]);
            //     await browser.newWindow("https://gmail.com")
            //     await browser.pause(6000);
            //     await browser.switchWindow("https://www.rahulshettyacademy.com/loginpagePractise/")
            //     await browser.pause(6000);


            // })




















        })

})
