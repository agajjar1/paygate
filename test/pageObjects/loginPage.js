class loginPage {

    get userName() {
        return $("#username"); // tag and attribute value css
    }

    get password() {
        return $("//input[@type='password']"); // by xpath
    }

    get signIn() {
        return $("#signInBtn");
    }

    async login(username, password) {
        await this.userName.setValue(username);
        await this.password.setValue(password);
        await this.signIn.click();
    }

}

module.exports = new loginPage();