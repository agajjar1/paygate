class membershipPage {

    get applyMembershipbtn() {
        return $("=Start Now!");
    }

    get verifyTabExist() {
        return $("[role='tab']");
    }
    get tabLocators() {
        return $$(".mat-step-text-label");
    }

    get submitBtn() {
        return $(".paygate-green-btn");
    }

    get toaster() {
        return $("#toast-container");
    }

    async getMembershipFormAttributes() {
        var formData = [];
        formData['company_name'] = '#mat-input-0';
        formData['company_address'] = '#mat-input-1';
        formData['primary_phone'] = '#mat-input-2';
        formData['secondary_phone'] = '#mat-input-3';
        formData['contact1_firstname'] = '#mat-input-4';
        formData['contact1_lastname'] = '#mat-input-5';
        formData['contact1_title'] = '#mat-input-6';
        formData['contact1_email'] = '#mat-input-7';
        formData['contact1_phone'] = '#mat-input-8';
        formData['contact2_firstname'] = '#mat-input-9';
        formData['contact2_lastname'] = '#mat-input-10';
        formData['contact2_title'] = '#mat-input-11';
        formData['contact2_email'] = '#mat-input-12';
        formData['contact2_phone'] = '#mat-input-13';
        return formData;
    }

}

module.exports = new membershipPage();